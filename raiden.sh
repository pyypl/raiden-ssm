#!/usr/bin/env bash

KEYSTORE=~/.ethereum/keystore
mkdir -p "$KEYSTORE"

echo "$ACCOUNT_KEYSTORE_JSON" > "$KEYSTORE"/account.json
echo "$ACCOUNT_PASSWORD" > "$KEYSTORE"/account.password

exec raiden \
  --address "$ACCOUNT_ADDRESS" \
  --password-file "$KEYSTORE"/account.password \
  --registry-contract-address "$CONTRACTS_REGISTRY" \
  --discovery-contract-address "$CONTRACTS_DISCOVERY" \
  --secret-registry-contract-address "$CONTRACTS_SECRET_REGISTRY" \
  "$@"
