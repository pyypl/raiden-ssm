#!/usr/bin/env bash

path=$1
shift

while read -r name value; do
  name=${name/"$path/"}
  name=${name^^}
  name=${name////_}
  name=${name//-/_}

  eval "export $name='$value'"
done < <(
  aws ssm get-parameters-by-path \
    --path "$path" \
    --recursive \
    --with-decryption \
    --query "Parameters[].[Name, Value]" \
    --output text
)

exec "$@"
